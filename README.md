<div align="center">
  <br>
    <img src="仓库图片/介绍.png" width="80%">

<h1>ComWeChat微信云崽快捷安装</h1>

</div>

# [本分支教程已经过时可前往dev分支查看更简单的教程](https://gitee.com/SHIKEAIXYY/Trss-ComWeChat-Yunzai/tree/dev)

# [点击查看视频教程](https://www.bilibili.com/video/BV1au4y177W3)

# QQ交流群：[778502891](https://qm.qq.com/cgi-bin/qm/qr?k=Ci29FjGxgrb_4fXitZfq-8nkhmFpQJMu&jump_from=webapi&authKey=fuo9h5XbOOzjb/t3yTl3vCxyPDXvrF63GSqC0CLySdOdPWi72DKe0PW3poFMFL9b)

# 说明

1. 本教程取自[时雨的Yunzai](https://gitee.com/TimeRainStarSky/Yunzai)协议端`ComWeChat`
2. 安装的bot在`redis-WXClient`上一级`Yunzai-Bot/`目录中
3. 安装了我已知插件库适配的全部插件包
4. 本`master分支`不如[dev分支](https://gitee.com/SHIKEAIXYY/Trss-ComWeChat-Yunzai/tree/dev/)
5. [ComWeChat项目](https://github.com/ljc545w/ComWeChatRobot)为PC Hook并且使用Com接口调用，故`只支持`Windows系统！！！
6. 如需lin或安卓部署可使用[时雨的微信项目](https://gitee.com/TimeRainStarSky/Yunzai-WeChat-Plugin)需提前安装云崽，安装可访问[→Gitee←](https://gitee.com/TimeRainStarSky/Yunzai)
7. 系统版本推荐用2019以上的（win10/11）别用win7/8.1

---

# 下载本库

## Gitee下载（需要提前下载[Git](https://registry.npmmirror.com/-/binary/git-for-windows/v2.43.0-rc0.windows.1/Git-2.43.0-rc0-64-bit.exe)）

```
git clone --depth 1 https://gitee.com/SHIKEAIXYY/Trss-ComWeChat-Yunzai

```

[![](https://img.shields.io/badge/ComWeChat-DeepSkyBlue)](https://github.com/JustUndertaker/ComWeChatBotClient)
[![](https://img.shields.io/badge/git-brightgreen.svg)](https://git-scm.com)
[![](https://img.shields.io/badge/node-yellow)](https://nodejs.cn)
[![](https://img.shields.io/badge/redis-red)](https://redis.io)
[![](https://img.shields.io/badge/Yunzai-red)](https://gitee.com/TimeRainStarSky/Yunzai)
[![](https://img.shields.io/badge/WeChat-red)](https://github.com/tom-snow/wechat-windows-versions)

---

# 安装node.js

1. 点开文件夹（安装包）进行安装(如果已有大于NodeJs18版本的可不安装)
<br>
    <img src="仓库图片/安装git和node.png" width="35%">

2. 下载`v3.7.0.30`版本的微信：[点击我进行下载](https://musetransfer.com/s/vfmdywhjj)

3. 登录微信（微信账号需要实名）后双击运行`禁用自动更新.exe`后可选择重新启动微信
<br>
    <img src="仓库图片/禁用自动更新.png" width="35%">

---

# 安装云崽Yunzai本体

1. 双击运行`安装pnpm.bat`，完成后双击运行`点击安装机器人.bat` 请确保node和git安装成功
打开 ComWeChat 双击运行`ComWeChat-Client-v0.0.8.exe`[点击下载vc库](https://learn.microsoft.com/zh-cn/cpp/windows/latest-supported-vc-redist?view=msvc-170)
<br>
    <img src="仓库图片/ComWeChat-Client-v0.0.8.png" width="35%">

2. 完成后打开`redis/双击我启动redis.bat`
<br>
    <img src="仓库图片/数据库.png" width="35%">

3. 完成后点击`启动机器人.bat`并查看日志输出是否连接成功

4. 成功后请给机器人发送`#设置主人`验证码在日志中蓝色字体

5. 如需安装插件可运行`一键安装插件.bat`进行全部下载(我)已知适配的插件

可能会缺少的依赖（yunzai根目录运行）：

```
pnpm add axios -w

```

# 关于低版本微信无法登录问题

这个问题原因是
 - tx会联网检测版本号
 - 当小于一定的值时就会去禁止我们登录
   - 假设这个值为5，当大于5时跳过，当小于5时就会禁止我们登录
 - 由此可知，为了防止无法登录我们有必要将版本号修改为大于tx的设定的值
   - 由于我们不知道这个值是多少，所以修改时可以修改为当前最新版微信版本号
   
## 解决办法

# [点击查看视频教程](https://www.bilibili.com/video/)

由上文可知原理，所以我们可以用`Cheat Engine`去进行修改

[点我下载Cheat Engine](https://www.cheatengine.org/downloads.php)

1. 下载完成后`点击`
<br>
    <img src="仓库图片/Cheat1.png" width="35%">

2. 打开`微信`（待扫码状态）

3. 选择`Processes`
<br>
    <img src="仓库图片/Cheat2.png" width="35%">

4. 找到`数字/字母-Wechat.exe`后点击
<br>
    <img src="仓库图片/Cheat3.png" width="35%">

5. 再点击`Open`
<br>
    <img src="仓库图片/Cheat4.png" width="35%">

6. `勾选Hex`后`替换000000`为`下方内容`即可(由于本教程使用的3.7.0.30版本微信，所以下方为该版本的16进制版本号，其他版本方法可以看视频设顶评论，或者问我)

```
6307001E
```
<br>
    <img src="仓库图片/Cheat5.png" width="35%">

7. 然后点击`FIrst Scan`
<br>
    <img src="仓库图片/Cheat6.png" width="35%">

8. 然后`ctrl+A全选`后`点击箭头`
<br>
    <img src="仓库图片/Cheat7.png" width="35%">

9. 然后继续`ctrl+A全选`下面的内容
<br>
    <img src="仓库图片/Cheat8.png" width="35%"> 

10. 然后`左键点击参数`修改为`下方内容`（使用最新3.9.11.17）

```
63090B11
```
<br>
    <img src="仓库图片/Cheat9.png" width="35%"> 

11. 然后点击`OK`

12. 保存修改数据（并非永久生效，重新登录需再次修改，本步骤是保存CT文件，方便下次进行修改）

<br>
    <img src="仓库图片/Cheat10.png" width="35%"> 